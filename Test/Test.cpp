#include "stdafx.h"

#include <windows.h>
#include <string>
#include <iostream>
#include <atlstr.h>
#include <atltime.h>

using namespace std;

/*string ExePath() {
	char buffer[MAX_PATH];
	GetModuleFileName( NULL, buffer, MAX_PATH );
	string::size_type pos = string( buffer ).find_last_of( "\\/" );
	return string( buffer ).substr( 0, pos);
}*/

int main() {
	//cout << "my directory is " << ExePath() << "\n";
	CString strNumber = _T("999");
	CStringA charstr(strNumber);

	int iNumber = 0;
	CTime startTime = CTime::GetCurrentTime();
	__time64_t lStart = startTime.GetTime();

	for(int i=0;i<10000;i++)
		iNumber = atoi(charstr);

	CTime endTime = CTime::GetCurrentTime();

	cout << "convert time = "<< (endTime.GetTime() - lStart);

	system("pause");
}
